#!/usr/bin/env node
"use strict";

process.title = "ocm";

// Grab provided args
const [,,...args] = process.argv;

const { bootstrap } = require("./bootstrap");

// Bootstraps the command line
bootstrap(args);
