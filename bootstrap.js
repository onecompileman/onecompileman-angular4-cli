const { CommandExtractor } = require("./commands/command-extractor");

const { Parser } = require("./utilities/argument-parser");

function bootstrap(args) {
  const parsedCommand = Parser.parse(args);
  const dirname = __dirname;
  const commandExtractor = new CommandExtractor(parsedCommand, dirname);
  commandExtractor.execute();
}

exports.bootstrap = bootstrap;
