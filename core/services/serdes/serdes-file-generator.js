const _ = require('lodash');

const { Stores } = require('../../../utilities/stores');

const { FileNameGenerator } = require('../../../utilities/file-name-generator');

const { SerdesModelBuilder } = require('./serdes-models-builder.service');

const { BlueprintFileGenerator } = require('../../../utilities/blueprint-file-generator');

const SerdesFileGenerator = (function (){

    function SerdesFileGenerator() {

    }

    SerdesFileGenerator.prototype.generateFiles = function(
        dirname,
        api
    ) {
        return new Promise(resolve => {
            const models = Stores.getInstance.models;
			const contents = SerdesModelBuilder.create(models, api);
			const generateSedes = BlueprintFileGenerator.generate(
				`${api}-api`,
				api,
				api,
				6,
				contents.serdes,
				'serdes',
				dirname,
				api
			);

			const generateUiri = BlueprintFileGenerator.generate(
				`${api}-api`,
				api,
				api,
				6,
				contents.uiri,
				'uiri',
				dirname,
				api
			);
			Promise.all([generateSedes, generateUiri]).then(res => {
                resolve(null);
            });
        })
    }

    return new SerdesFileGenerator();
})();

exports.SerdesFileGenerator = SerdesFileGenerator;