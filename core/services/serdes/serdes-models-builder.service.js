const _ = require('lodash');

const { FileNameGenerator } = require('../../../utilities/file-name-generator');

const SerdesModelBuilder = (function() {

    function SerdesModelBuilder() {

    }

    SerdesModelBuilder.prototype.create = function (models, api){
        return models.reduce((acc, model) => {
            const relativeDir = '../'.repeat(model.path.split('/').length);
            const importString = `import { ${model.modelName} } from '../../../app/shared/models/${model.path}/${model.fileName}.model';\n`;

            acc.serdes.imports += importString;
            acc.uiri.imports += importString;
            acc.serdes.models_array += `
            {
                interface: ${model.modelName},
                route: '${FileNameGenerator.transform(model.modelName)}',
                serdes: true
            },`;
            acc.uiri.routes_array += model.serviceMethods.reduce((ass, method) => {
               return (
					ass +
					`
            {
                uiri: {
                  success: false,
                  error: true
                },
                interface: ${model.modelName},
                route: '${FileNameGenerator.transform(model.modelName)}',
                operation: '${method.method}',
                uiOptions: {
                message: {
                    error: 'Failed!',
                    success: 'Success!',
                    title: 'Profitcenter',
                    apiError: {
                    enabled: true,
                    translation: true
                    }
                }
                }
            },`
				);
            }, '');

            return acc;
        }, {
            serdes: {
                imports: '',
                models_array: '',
                name: api.toUpperCase()
            },
            uiri: {
                routes_array: '',
                imports: '',
                name: api.toUpperCase()
            }
        });
    }

    return new SerdesModelBuilder();

})();

exports.SerdesModelBuilder = SerdesModelBuilder;