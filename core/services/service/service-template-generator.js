const {
  ServicePropertiesBuilderService
} = require('./service-properties-builder.service');
const {
  BlueprintFileGenerator
} = require('../../../utilities/blueprint-file-generator');

const ServiceTemplateGenerator = (function() {
  function ServiceTemplateGenerator() {}

  ServiceTemplateGenerator.prototype.generate = function(modelObj, dirname, version, api = null) {
    return new Promise((resolve, reject) => {
      BlueprintFileGenerator.generate(
			modelObj.fileName,
			modelObj.path,
			modelObj.modelName,
			version,
			{
				...ServicePropertiesBuilderService.create(modelObj),
				name: modelObj.modelName,
				api_import: `import { ${api.toUpperCase()}_API } from ${
					version === 4
						? "'../../../../../config/requests/" +
						  api.toLowerCase() +
						  '/' +
						  api.toLowerCase() +
						  "-api.config';"
						: "'../../../../../config/01-requests/" +
						  api.toLowerCase() +
						  '/' +
						  api.toLowerCase() +
						  "-api.config';"
				}`,
				api: `${api.toUpperCase()}_API`,
			},
			'data-service',
			dirname,
			api
		).then(res => resolve(res));
    });
  };

  return new ServiceTemplateGenerator();
})();

exports.ServiceTemplateGenerator = ServiceTemplateGenerator;
