const _ = require('lodash');

const { TypeIdentifier } = require('../../../utilities/type-identifier');

const ServicePropertiesBuilderService = (function() {
    
    function ServicePropertiesBuilderService() {}

    const METHODS = {
        put: '.customPUT(',
        get1: '.get();',
        get: '.getList();',
        delete: '.remove();',
        post: '.post('
    };

    /**path, methodName, parameters, model, method */
    ServicePropertiesBuilderService.prototype.create = function(modelsObj) {
        return modelsObj.serviceMethods.reduce((acc, serviceMethod, i) => {

      
            const { path, parameters, method, methodName } = serviceMethod;            
            const param = _.find(parameters, { in: 'body' });            
            const restangularObj = restangularObjBuilder(modelsObj, path, method, param);
            const { restangularString, isArray } = restangularObj;
            const { importModels, parameterString } = methodParameterBuilder(parameters);

            importModels.forEach((imports) => {
                const index = _.findIndex(acc.modelImports, { modelName: imports.modelName });
                if (index === -1) {
                    const relativeDir =  '../'.repeat(modelsObj.path.split('/').length);
                    acc.imports += `import { ${imports.modelName} } from '${relativeDir}../../shared/models/${imports.path}/${imports.fileName}.model';\n`;
                    acc.modelImports.push(imports);
                }
            }); 

            if(acc.modelImports.length === 0 && i === modelsObj.serviceMethods.length - 1) {
                const relativeDir = '../'.repeat(modelsObj.path.split('/').length);
                acc.imports += `import { ${modelsObj.modelName} } from '${relativeDir}../../shared/models/${modelsObj.path}/${modelsObj.fileName}.model';\n`;     
            }

            const isArrays = (isArray && 
                             method !== 'delete' && 
                             method !== 'put' && 
                             method !== 'post') ? '[]':'';

            const returnType = `Observable<${(method === 'delete') ? 'void':modelsObj.modelName}${isArrays}>`
            
            acc.methods += `\n  ${methodName}(${parameterString}): ${returnType} {\n    return ${restangularString}\n  }\n`;

            return acc;
        },{
            imports: '',
            methods: '',
            modelImports: []
        })
    };

    //Private methods
    function restangularObjBuilder(model, path, method, param) {
        const paths = trimSlashes(path).split('/');
		let restangularString = 'this.restangular';
        let hasOne = false;
		for (let i = 0; i < paths.length; i++) {
			if (i < paths.length - 1) {
				if (hasOne = paths[i + 1].includes('{')) {
                    restangularString += `.one('${paths[i]}', ${paths[i + 1].substring(1, paths[i + 1].length - 1)})`;
					i++;
					continue;
				} else {
                    hasOne = false;
                }
            }
            hasOne = false;
			restangularString += `.all('${paths[i]}')`;
		}

		if (method === 'put' || method === 'post') {
            restangularString += METHODS[method] + camelCase((typeof param !== 'undefined') ? param.model.modelObj.modelName : model.modelName) + ' );';
        } else if (method === 'get') {
            restangularString += METHODS[method + (hasOne ? '1':'')];
        } else {
            restangularString += METHODS[method];
        }
        
        return { isArray: !hasOne, restangularString };
    }

    function trimSlashes(path) {
        path = (path.charAt(0) === '/') ? path.substring(1) : path;
        path =(path.charAt(path.length - 1) === '/') ? 
                path.substring(0,path.length - 1) : path;
        return path;
    }

    function methodParameterBuilder(parameters) {
        const filteredParameters = parameters.filter(param => {
            return param.in !== 'query';
        });
        return filteredParameters.reduce((acc, param, i) => {
            if (param.in === 'body') {
                acc.parameterString += ` ${camelCase(param.model.modelObj.modelName)}: ${param.model.modelObj.modelName}${param.model.isArray ? '[]':''}`;
                acc.importModels.push(param.model.modelObj);
            } else {
                acc.parameterString += `${param.name}: ${TypeIdentifier.identify(param.type)}`;
            }
                acc.parameterString += i === filteredParameters.length - 1 ? '' : ', '; 
            return acc;
        }, {
            parameterString: '',
            importModels: []
        })
    }

    function camelCase(s) {
		return s[0].toLowerCase() + s.slice(1);
	}



    return new ServicePropertiesBuilderService();

})();

exports.ServicePropertiesBuilderService = ServicePropertiesBuilderService;