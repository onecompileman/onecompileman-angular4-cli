const _ = require('lodash');

const { Stores } = require('../../../utilities/stores');

const { FileNameGenerator } = require('../../../utilities/file-name-generator');

const { ServiceTemplateGenerator } = require('./service-template-generator');

const {
  IndexFileGenerator
} = require('../../../utilities/index-file-generator');

const ServiceFileGenerator = (function() {
  function ServiceFileGenerator() {}

  ServiceFileGenerator.prototype.generateFiles = function(
    servicesObj,
    dirname,
    version,
    api
  ) {
    return new Promise((resolve, reject) => {
      const paths = Object.keys(servicesObj);
      const models = Stores.getInstance.models;
      const indexContents = {
        import_statements: '',
        export_statements: '',
        exports: ''
      };

      const services = convertPathsToServices(paths, servicesObj, models);

      Stores.getInstance.services = services;

      Stores.getInstance.models = models.map((model, i) => {
        indexContents.exports += `\n${model.modelName}Service ${
          i === models.length - 1 ? '' : ','
        }`;
        indexContents.export_statements += `export * from './${model.path}/${
          model.fileName
        }.service';\n`;
        indexContents.import_statements += `import { ${
          model.modelName
        }Service } from './${model.path}/${model.fileName}.service';\n`;
        model['serviceMethods'] = _.filter(services, {
          model: { fullPath: model.fullPath }
        });
        return model;
      });

      const generateServices = Stores.getInstance.models.map(model =>
        ServiceTemplateGenerator.generate(model, dirname, version, api)
      );
      ;
      generateServices.push(...((version === 4) ? [IndexFileGenerator.generate(indexContents, 'service', dirname)] : []));

      Promise.all([
        ...generateServices
      ]).then(res => {
        resolve(res);
      });
    });
  };

  //Private Methods
  function convertPathsToServices(paths, servicesObj, models) {
    return paths.reduce((acc, path) => {
      const httpMethods = Object.keys(servicesObj[path]);
      let model = null;
      acc = [
        ...acc,
        ...httpMethods.map(method => {
          const methodObj = servicesObj[path][method];
          const methodName = methodObj['x-method-name'];

          methodObj['parameters'] = methodObj['parameters'] || [];

          const parameters = methodObj.parameters.map(param => {
            if (param.hasOwnProperty('schema')) {
              if (param.schema.hasOwnProperty('items')) {
                const modelPaths = param.schema.items.$ref
					          .split('/')[2]
					          .split('.')
                    .map((path, i) => {
                      return FileNameGenerator.transform(path);
                    });
                const fullPath = modelPaths.toString().replace(/,/g, '/');
                const modelObj = _.find(models, { fullPath: fullPath });
                param['model'] = { modelObj, isArray: true };
              } else {
                const modelPaths = param.schema.$ref
                  .split('/')[2]
                  .split('.')
                  .map((path, i) => {
                    return FileNameGenerator.transform(path);
                  });

                const fullPath = modelPaths.toString().replace(/,/g, '/');
                const modelObj = _.find(models, { fullPath: fullPath });
                param['model'] = { modelObj, isArray: false};
              }
            }
            return param;
          });

          //Identifying the model
          model = getMethodModel(methodObj, models) || model;

          return { path, methodName, parameters, model, method };
        })
      ];
      return acc;
    }, []);
  }

  function getMethodModel(methodObj, models) {
    let model = null;
    const successResponses = ['200', '201', '204', '202', '203'];
    successResponses.forEach(resp => {
      const { responses } = methodObj;
      if (responses.hasOwnProperty(resp)) {
        if (responses[resp].hasOwnProperty('schema')) {
          let modelPaths = responses[resp].schema.hasOwnProperty('items')
            ? responses[resp].schema.items.$ref.split('/')[2].split('.')
            : responses[resp].schema.$ref.split('/')[2].split('.');

          modelPaths = modelPaths.map((path, i) => {
            return FileNameGenerator.transform(path);
          });

          const fullPath = modelPaths.toString().replace(/,/g, '/');
          model = _.find(models, { fullPath: fullPath });
        }
      }
    });
    return model;
  }

  return new ServiceFileGenerator();
})();

exports.ServiceFileGenerator = ServiceFileGenerator;
