const {
  ModelTemplateGenerator
} = require('./model-template-generator.factory');

const { Stores } = require('../../../utilities/stores');

const { FileNameGenerator } = require('../../../utilities/file-name-generator');

const ModelsFileGenerator = (function() {
  function ModelsFileGenerator() {}

  ModelsFileGenerator.prototype.generateFiles = function(
    modelsObj,
    dirname,
    version,
    api
  ) {
    return new Promise((resolve, reject) => {
      const modelsName = Object.keys(modelsObj);

      const models = modelsName.map(model => {
        const leadingName = !model
			.split('.')
			[model.split('.').length - 1].toLowerCase()
			.includes(model.split('.')[0].toLowerCase()) ? capitalizeFirstLetter(model.split('.')[0]) : '';

        const modelName = leadingName + model.split('.')[model.split('.').length - 1];

        const modelPaths = model.split('.').map((modelPaths, i) => {
          return FileNameGenerator.transform(modelPaths);
        });

        const fileName =  modelPaths[modelPaths.length - 1].split('.')[0];

        modelPaths.pop();

        const path = modelPaths
          .slice(modelPaths.length - 3)
          .toString()
          .replace(/,/g, '/');

        return {
			fileName: fileName,
			modelName: modelName,
			path: (api === null ? '' : api + '/') + path,
			fullPath: `${path}/${fileName}`,
			jsonObj: modelsObj[model],
		};
      });

      Stores.getInstance.models = models;

      const generateModels = models.map(model => {
        const { fileName, modelName, path, jsonObj } = model;
        return ModelTemplateGenerator.generate(
          fileName,
          modelName,
          path,
          jsonObj,
          dirname,
          models,
          version,
          api
        );
      });

      Promise.all(generateModels).then(data => {
        resolve(null);
      });
    });
  };

  function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
  };

  return new ModelsFileGenerator();
})();

exports.ModelsFileGenerator = ModelsFileGenerator;
