const {
  ModelPropertiesBuilder
} = require('./model-properties-builder.service');
const { BlueprintFileGenerator } = require('../../../utilities/blueprint-file-generator');

const ModelTemplateGenerator = (function() {
  function ModelTemplateGenerator() {}

  ModelTemplateGenerator.prototype.generate = function(fileName, name, path, jsonObj, dirname, models, version, api) {
    const contents = {
        ...ModelPropertiesBuilder.create(path, jsonObj.properties, models), 
        name
      };
    BlueprintFileGenerator.generate(fileName, path, name, version, contents, 'model', dirname, api);
  };

  return new ModelTemplateGenerator();
})();

exports.ModelTemplateGenerator = ModelTemplateGenerator;
