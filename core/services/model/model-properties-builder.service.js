const { TypeIdentifier } = require('../../../utilities/type-identifier');

const _ = require('lodash'); 

const ModelPropertiesBuilder = (function() {
  function ModelPropertiesBuilder() {}

  ModelPropertiesBuilder.prototype.create = function(folderPath, properties, modelsObj) {
    const propertyNames = Object.keys(properties);

    return propertyNames.reduce(
      (acc, propertyName, i) => {
        const s = (i > 0) ? '\n':''; 
        if (properties[propertyName].hasOwnProperty("$ref")) {
          const nameSplit = properties[propertyName].$ref.split('.');
          const leadingName = !nameSplit[nameSplit.length - 1]
				.toLowerCase()
				.includes(
					nameSplit[0].replace('#/definitions/', '').toLowerCase()
				) ? capitalizeFirstLetter(nameSplit[0].replace('#/definitions/', '')) : '';
          const name = leadingName + nameSplit[nameSplit.length - 1];
          const { fileName, modelName, path } = _.find(modelsObj, { modelName: name });
          const relativeDir = (folderPath === path) ? './' : '../'.repeat(folderPath.split('/').length) + path + '/';

          acc.properties += `${s}  ${propertyName}: ${name} = null;`;
         
          acc.imports += `import { ${modelName} } from '${relativeDir}${fileName}.model';\n`;

        } else if (properties[propertyName].hasOwnProperty("items")) { 
          if (properties[propertyName].items.hasOwnProperty('type')) {
              acc.properties += `${s}  ${propertyName}: ${properties[propertyName].items.type}[] = null;`;
          } else {
            const nameSplit = properties[propertyName].items.$ref.split('.');
            const leadingName = !nameSplit[nameSplit.length - 1]
                .toLowerCase()
                .includes(nameSplit[0].replace('#/definitions/', '').toLowerCase())
                ? capitalizeFirstLetter(nameSplit[0].replace('#/definitions/', ''))
                : '';
            const name = leadingName + nameSplit[nameSplit.length - 1];
            const { fileName, modelName, path } = _.find(modelsObj, { modelName: name });
            const relativeDir = folderPath === path ? './' : '../'.repeat(folderPath.split('/').length) + path + '/';

            acc.properties += `${s}  ${propertyName}: ${name}[] = null;`;
            acc.imports += `import { ${modelName} } from '${relativeDir}${fileName}.model';\n`;
          }
          
        } else {
          acc.properties += `${s}  ${propertyName}: ${TypeIdentifier.identify(properties[propertyName].type)} = null;`;
        }

        return acc;
      },
      {
        properties: "",
        imports: ""
      }
    );
  };

  function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
  };

  return new ModelPropertiesBuilder();
})();

exports.ModelPropertiesBuilder = ModelPropertiesBuilder;
