import { Injectable, Inject } from '@angular/core';

import { Restangular } from 'ngx-restangular';
import { Observable } from 'rxjs/Observable';
<% imports %>
<% api_import %>

@Injectable()
export class <% name %>Service {
    
  constructor(@Inject(<% api %>) private restangular: Restangular) {}
  <% methods %>

}
