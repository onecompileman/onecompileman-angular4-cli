import { Injectable, Inject } from '@angular/core';

import { Restangular } from 'ngx-restangular';
import { Observable } from 'rxjs';
<% imports %>
<% api_import %>

@Injectable({
  providedIn: 'root'
})
export class <% name %>DataService {

  constructor(@Inject(<% api %>) private restangular: Restangular) {}
  <% methods %>

}
