import { UiriConfigVal } from '@ffufm/ngx-uiri';
<% imports %>

export const <% name %>_API_UIRI_CONFIG: UiriConfigVal[] = [
<% routes_array %>
];
