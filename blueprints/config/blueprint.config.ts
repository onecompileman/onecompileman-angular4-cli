import { InjectionToken } from '@angular/core';
import { environment } from '../../environments/environment';
import { Restangular } from 'ngx-restangular';
import { AuthService } from '../02-auth/auth.service';

export const <% inject %> = new InjectionToken<any>('<% api_name %>.api.restangular.config');

export function Restangular<% name %>ApiFactory(restangular: Restangular, authService: AuthService) {

	return restangular.withConfig(RestangularConfigurer => {
		
		RestangularConfigurer.setRequestSuffix('/');
		RestangularConfigurer.setBaseUrl(
			//api path
		);
	});
}
