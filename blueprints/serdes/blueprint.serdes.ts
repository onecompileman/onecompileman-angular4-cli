import { SerdesConfigVal } from '@ffufm/ngx-serdes';
<% imports %>

export const <% name %>_API_SERIALIZATION_CONFIG: SerdesConfigVal[] = [
<% models_array %>
];
