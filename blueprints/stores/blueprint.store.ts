import { Injectable } from '@angular/core';

import { <% name %> } from '../../shared/models/<% path_name %>.model';
import { <% name %>Service } from '../services/<% path_name %>.service';

@Injectable()
export class <% name %>Store {

    <% methods %>

}
