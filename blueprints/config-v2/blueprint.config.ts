import { InjectionToken } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Restangular } from 'ngx-restangular';
import { SerdesService } from '@ffufm/ngx-serdes';
import { <% name %>_API_SERIALIZATION_CONFIG } from './<% api_name %>-api.serdes';
import { UiriService } from '@ffufm/ngx-uiri';
import { <% name %>_API_UIRI_CONFIG } from './<% api_name %>-api.uiri';

export const <% name %>_API = new InjectionToken<any>('<% api_name %>.api.restangular.config');

export function Restangular<% title %>ApiFactory(restangular: Restangular, serdes: SerdesService, uiri: UiriService): void {
  return restangular.withConfig(restangularConfigurer => {
    restangularConfigurer.setBaseUrl(environment.api.<% api_name %>);
    serdes.register(<% name %>_API_SERIALIZATION_CONFIG, restangularConfigurer);
    uiri.register(<% name %>_API_UIRI_CONFIG, restangularConfigurer);
	});
}
