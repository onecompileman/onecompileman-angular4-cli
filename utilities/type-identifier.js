const TypeIdentifier = (function() {
    function TypeIdentifier() {}

    TypeIdentifier.prototype.identify = function(propertyType) {
		switch (propertyType) {
			case 'integer':
			case 'float':
			case 'double':
			case 'number':
				return `number`;
			case 'string':
				return `string`;
			case 'boolean':
				return `boolean`;
		}
    };
    
    return new TypeIdentifier();

})();

exports.TypeIdentifier = TypeIdentifier;