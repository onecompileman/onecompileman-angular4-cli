const FileNameGenerator = (function() {
  function FileNameGenerator() {}

  FileNameGenerator.prototype.transform = function(name) {
    let newName = '';
    for (let i = 1; i < name.length; i++) {
      const chrCode = name.charCodeAt(i);
      const chr = name.charAt(i).toLowerCase();

      newName += (chrCode >= 65 && chrCode <= 90) ?
                  `-${chr}` : chr; 
    }
    return name.charAt(0).toLowerCase() + newName;
  };

  return new FileNameGenerator();
})();

exports.FileNameGenerator = FileNameGenerator;