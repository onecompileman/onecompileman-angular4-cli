const { BLUEPRINT_TYPE } = require('./constants');

const FileSystem = require('fs');
const mkdirp = require('mkdirp');

const { throwError } = require('./errors');
const { BlueprintFileExtractor } = require('./blueprint-file-extractor');

const IndexFileGenerator = (function() {

    function IndexFileGenerator() {}

    IndexFileGenerator.prototype.generate = function(contents, type, dirname) {
        return new Promise((resolve, reject) => {
            BlueprintFileExtractor.extract(`${dirname}${BLUEPRINT_TYPE[type].indexPath}`)
                    .then(blueprint => {
						

                const keyToReplace = BLUEPRINT_TYPE[type].indexContents;

				const fileContents = keyToReplace.reduce((acc, key) => {
					acc = acc.replace('<% ' + key + ' %>', contents[key]);
					return acc;
				}, blueprint);

                const { folderStructure } = BLUEPRINT_TYPE[type];

                mkdirp(`${folderStructure}`, function(err) {
				    if (err) console.error(err);

					FileSystem.writeFile(
					    `${folderStructure}/index.ts`,
						fileContents.trim(),
						'utf-8',
						error => {
							if (error) {
						    	throwError(error);
								reject(error);
							}
							resolve(null);
						});
					});
            })
        });
    };

    return new IndexFileGenerator();

})();

exports.IndexFileGenerator = IndexFileGenerator;