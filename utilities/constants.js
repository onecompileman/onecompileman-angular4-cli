/**
 * Constants for Blueprints type
 */
const BLUEPRINT_TYPE = {
	model: {
		pathv4: '/blueprints/models/v4/blueprint.model.ts',
		pathv6: '/blueprints/models/v6/blueprint.model.ts',
		contents: ['imports', 'name', 'properties'],
		folderStructure: './src/app/shared/models/',
	},
	"data-service": {
		pathv4: '/blueprints/services/v4/blueprint.service.ts',
		pathv6: '/blueprints/services/v6/blueprint.service.ts',
		contents: ['imports', 'name', 'methods', 'api', 'api_import'],
		folderStructure: './src/app/core/data-services/',
		indexContents: ['import_statements', 'exports', 'export_statements'],
		indexPath: '/blueprints/services/blueprint-index.ts',
	},
	store: {
		path: '/blueprints/stores/blueprint.store.ts',
		contents: ['import', 'name', 'methods'],
		folderStructure: './src/app/core/stores/',
	},
	serdes: {
		pathv4: '/blueprints/serdes/blueprint.serdes.ts',
		pathv6: '/blueprints/serdes/blueprint.serdes.ts',
		contents: ['imports', 'models_array', 'name'],
		folderStructure: './src/config/01-requests/',
	},
	config: {
		pathv4: '/blueprints/config/blueprint.config.ts',
		pathv6: '/blueprints/config-v2/blueprint.config.ts',
		contents: ['api_name', 'inject', 'name', 'title'],
		folderStructure: './src/config/01-requests/'
	},
	uiri: {
		pathv4: '/blueprints/uiri/blueprint.uiri.ts',
		pathv6: '/blueprints/uiri/blueprint.uiri.ts',
		contents: ['routes_array', 'name', 'imports'],
		folderStructure: './src/config/01-requests/'
	}
};

exports.BLUEPRINT_TYPE = BLUEPRINT_TYPE;
