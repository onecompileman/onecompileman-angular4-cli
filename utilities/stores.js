const _ = require('lodash');

const Stores = (function() {
    
    var instance;

    function Stores() {
        this.models = [];
        this.services = [];
        return this;
    }

    Stores.prototype.initModels = function(models) {
        this.models = models;
    };

    Stores.prototype.addModel = function(model) {
        this.models.push(model);
    };

    Stores.prototype.getModelByName = function(name) {
        return _.find(this.models, { modelName: name });
    };

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Stores();   
            }
            return instance;
        }
    }
})();

exports.Stores = Stores;