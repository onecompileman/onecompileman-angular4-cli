const FileSystem = require("fs");

const { throwError } = require("./errors");

const SwaggerFileExtractor = (function() {
  function SwaggerFileExtractor() {}

  SwaggerFileExtractor.prototype.extractFile = function(path) {
    return new Promise((resolve, reject) => {
      if (!path.includes(".json")) {
        throwError("File declared is not JSON!!");
      }
      
      FileSystem.readFile(path, 'utf8', function(err, data) {
        if (err) throwError(err);
        resolve(data);
		  });
    });
  };

  return new SwaggerFileExtractor();
})();

exports.SwaggerFileExtractor = SwaggerFileExtractor;
