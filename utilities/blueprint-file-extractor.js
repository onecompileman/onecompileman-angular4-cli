const FileSystem = require('fs');

const { throwError } = require('./errors');

const BlueprintFileExtractor = (function() {
  function BlueprintFileExtractor() {}

  /**
   * Extracts blueprints content
   * @param string path
   * @return Promise<string>
   */
  BlueprintFileExtractor.prototype.extract = function(path) {
    return new Promise((resolve, reject) => {
       FileSystem.readFile(path, 'utf8', function(err, data) {
			if (err) {
				throwError(err);
			}
			// FileSystem.close(file);
			resolve(data);
      });
    });
  };

  return new BlueprintFileExtractor();
})();

exports.BlueprintFileExtractor = BlueprintFileExtractor;