const FileSystem = require("fs");
const mkdirp = require('mkdirp');

const { throwError } = require("./errors");
const { BlueprintFileExtractor } = require("./blueprint-file-extractor");

const { BLUEPRINT_TYPE } = require('../utilities/constants');

const BlueprintFileGenerator = (function() {
  /**
   * BlueprintFileGenerator constructor
   */
  function BlueprintFileGenerator() {}

  /**
   * @param string name
   * @param string contents
   * @param string type
   */
  BlueprintFileGenerator.prototype.generate = function(fileName, path, name, version, contents, type, dirname, api = null) {
    return new Promise((resolve, reject) => {
        const blueprintPath = version === 4 ? BLUEPRINT_TYPE[type].pathv4 : BLUEPRINT_TYPE[type].pathv6;
        BlueprintFileExtractor.extract(`${dirname}${blueprintPath}`).then(blueprint => {

        const keyToReplace = BLUEPRINT_TYPE[type].contents;

        const fileContents = keyToReplace.reduce((acc, key) => {
          const pattern = new RegExp('<% ' + key + ' %>', 'g');
          acc = acc.replace(pattern, contents[key]);
          return acc;
        }, blueprint);

        const { folderStructure } = BLUEPRINT_TYPE[type];

        mkdirp(`${folderStructure}${path}`, function(err) {
          if (err) console.error(err);
          const filePath =  (!!api) ? `${folderStructure}${path}/${fileName}.${type}.ts`
                                    : `${folderStructure}${path}/${fileName}.${type}.ts`
          FileSystem.writeFile(
            filePath,
            fileContents.trim() + '\n',
            'utf-8',
            error => {
              if (error) {
                throwError(error);
                reject(error);
              }
              resolve(null);
            }
          );
        });
      });
    });

  };
  /**
   * returns a singleton instance of BlueprintFileGenerator
   */
  return new BlueprintFileGenerator();
})();

exports.BlueprintFileGenerator = BlueprintFileGenerator;