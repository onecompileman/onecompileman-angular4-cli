const figlet = require('figlet');
const clui = require('clui');
const chalk = require('chalk');

const { throwError } = require('../utilities/errors');
const { CommandValidator } = require('./commands-validator');
const { SwaggerFileExtractor } = require('../utilities/swagger-file-extractor');
const { ModelsFileGenerator } = require('../core/services/model/models-file-generator');
const { ServiceFileGenerator } = require('../core/services/service/service-file-generator');
const { SerdesFileGenerator } = require('../core/services/serdes/serdes-file-generator');
const { BlueprintFileGenerator } = require('../utilities/blueprint-file-generator');
const { FileNameGenerator } = require('../utilities/file-name-generator');


const CommandExtractor = (function() {
  function CommandExtractor(commandsObj, dirname) {
    if (typeof commandsObj === 'undefined') {
      throwError('No commands passed!!');
    }
    this.commandsObj = commandsObj;
    this.dirname = dirname;
    this.version = commandsObj.version || 4;
    this.api = commandsObj.api;
  }

  CommandExtractor.prototype.execute = function() {
    if (CommandValidator.validate(this.commandsObj)) {


		const Spinner = clui.Spinner;
    const countdown = new Spinner('Generating models and services....');
    let i = 0;
    countdown.start();
    setTimeout(() => {
            SwaggerFileExtractor.extractFile(this.commandsObj.swagger).then(fileContents => {
					  const swaggerContent = JSON.parse(fileContents);
					  ModelsFileGenerator.generateFiles(swaggerContent.definitions, this.dirname, this.version, this.api).then(res => {
              ServiceFileGenerator.generateFiles(swaggerContent.paths, this.dirname, this.version, this.api).then(r => {
                BlueprintFileGenerator.generate(`${this.api}-api`, this.api, `${this.api.toUpperCase()}_API`,	this.version,
                    {
                      inject: `${this.api.toUpperCase()}_API`,
                      name: `${this.api.toUpperCase()}`,
                      api_name: this.api.toLowerCase(),
                      title:  this.api.replace(
                      /\w\S*/g, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    }
        )
					},
					'config',
					this.dirname,
					this.api
				).then(ss => {
            SerdesFileGenerator.generateFiles(this.dirname, this.api).then(res => {
              setTimeout(() => {
                figlet('onecompileman (c)', function(err, data) {
                  if (err) {
                    console.log('Something went wrong...');
                    console.dir(err);
                    return;
                  }
                  console.log(data);
                  console.log(
                    chalk.default.green(
                      'Created by: Stephen Vinuya\nLinkedIn: https://www.linkedin.com/in/stephen-vinuya-54441b106/'
                    )
                  );
                  process.stdout.write('\n');
                  process.exit(0);
                });
              }, 1000);
            });

        });
              //   if (this.version === 6) {
              //   SerdesFileGenerator.generateFiles(swaggerContent.paths, this.dirname, this.api).then(r => {
              //     setTimeout(() => {
              //       figlet('onecompileman (c)', function(err, data) {
              //         if (err) {
              //           console.log('Something went wrong...');
              //           console.dir(err);
              //           return;
              //         }
              //         console.log(data);
              //         console.log(
              //           chalk.default.green(
              //             'Created by: Stephen Vinuya\nLinkedIn: https://www.linkedin.com/in/stephen-vinuya-54441b106/'
              //           )
              //         );
              //         process.stdout.write('\n');
              //         process.exit(0);
              //       });
              //     }, 1000);
              //   });
              // } else {
              //   setTimeout(() => {
              //     figlet('onecompileman (c)', function(err, data) {
              //       if (err) {
              //         console.log('Something went wrong...');
              //         console.dir(err);
              //         return;
              //       }
              //       console.log(data);
              //       console.log(
              //         chalk.default.green(
              //           'Created by: Stephen Vinuya\nLinkedIn: https://www.linkedin.com/in/stephen-vinuya-54441b106/'
              //         )
              //       );
              //       process.stdout.write('\n');
              //       process.exit(0);
              //     });
				      // }, 1000);
              // }
              });
            }
					  );
        });
    },2000);
    }
    return;
  };

  return CommandExtractor;
})();

exports.CommandExtractor = CommandExtractor;
