const _ = require('lodash');

const { throwError } = require('../utilities/errors');

const commands = require('./collection.json');

const { aliases } = commands;
const filteredCommands = Object.keys(aliases).reduce((acc, alias) => {
  acc.push(alias, aliases[alias]);
  return acc;
}, []);

const CommandValidator = (function() {
  function CommandValidator() {}

  CommandValidator.prototype.validate = function(commandObj) {
    const commandRoot =
      commandObj['_'] instanceof Array ? commandObj['_'] : [commandObj['_']];

    const removedExistingCommands = commandRoot.filter(
      a => filteredCommands.indexOf(a) === -1
    );

    if (commandRoot.includes('ls') || commandRoot.includes('list')) {
      const listOfCommands = Object.keys(commands.commands).reduce(
        (acc, command) => acc + "\n\n" + commands.commands[command].help,
        ''
      );
      console.log(listOfCommands);
      return false;
    }

    if (commandRoot.includes('v') || commandRoot.includes('version')) {
      console.log(commands.commands.version);
      return false;
    }

		if (removedExistingCommands.length > 0) {
			throwError(commands.error);
			return false;
		}

    if(!commandObj.hasOwnProperty('api')) {
      throwError('No api name declared!!');
      return false;
    }

    if (!commandObj.hasOwnProperty('swagger')) {
      throwError('No swagger argument declared!!');
      return false;
    }

    return true;
  };

  return new CommandValidator();
})();

exports.CommandValidator = CommandValidator;
